import Vuex from 'vuex'
import Vue from 'vue'
Vue.use(Vuex)

const state = () => ({
  
})

const getters = {
}

const mutations = {
 
}

const actions = {

}
export default {
  state,
  getters,
  actions,
  mutations,
}
